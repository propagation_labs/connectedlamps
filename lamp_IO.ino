// Adafruit IO Digital Output Example
// Tutorial Link: https://learn.adafruit.com/adafruit-io-basics-digital-output
//
// Adafruit invests time and resources providing this open source code.
// Please support Adafruit and open source hardware by purchasing
// products from Adafruit!
//
// Written by Todd Treece for Adafruit Industries
// Copyright (c) 2016 Adafruit Industries
// Licensed under the MIT license.
//
// All text above must be included in any redistribution.

/************************** Configuration ***********************************/

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define NUMPIXELS 16 // Popular NeoPixel ring size


// edit the config.h tab and enter your Adafruit IO credentials
// and any additional configuration needed for WiFi, cellular,
// or ethernet clients.
#include "config.h"

/************************ Example Starts Here *******************************/

// digital pin 5
#define LED_PIN 5

// set up the 'digital' feed
AdafruitIO_Feed *digital = io.feed("digital");


Adafruit_NeoPixel pixels(NUMPIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);

const int buttonPin = 4;     // the number of the pushbutton pin
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
    pinMode(buttonPin, INPUT);


    // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

 // pinMode(LED_PIN, OUTPUT);
      pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

  // start the serial connection
  Serial.begin(115200);

  // wait for serial monitor to open
  while(! Serial);

  // connect to io.adafruit.com
  Serial.print("Connecting to Adafruit IO");
  io.connect();

  // set up a message handler for the 'digital' feed.
  // the handleMessage function (defined below)
  // will be called whenever a message is
  // received from adafruit io.
  digital->onMessage(handleMessage);

  // wait for a connection
  while(io.status() < AIO_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  // we are connected
  Serial.println();
  Serial.println(io.statusText());
  digital->get();



}

void loop() {
      //pixels.setPixelColor(8, pixels.Color(128, 0, 128));


  // io.run(); is required for all sketches.
  // it should always be present at the top of your loop
  // function. it keeps the client connected to
  // io.adafruit.com, and processes any incoming data.
  io.run();

  buttonState = digitalRead(buttonPin);
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {

    // normal state
  } else {
    digital->save(true);
    // tapped!  
    
  }

}

// this function is called whenever an 'digital' feed message
// is received from Adafruit IO. it was attached to
// the 'digital' feed in the setup() function above.
void handleMessage(AdafruitIO_Data *data) {
  

  Serial.print("received <- ");

  if(data->toPinLevel() == HIGH) {
    Serial.println("HIGH");
    colorIncrease();
    colorDecrease();
    colorIncrease();
    colorDecrease();
    colorIncrease();
    colorDecrease();
    colorIncrease();
    colorDecrease();
    colorIncrease();
    colorDecrease();
        // send false to reset
        digital->save(false);


  }
  else {
    Serial.println("LOW");
      pixels.clear(); // Set all pixel colors to 'off'
      pixels.show();   // Send the updated pixel colors to the hardware.

  }


 // digitalWrite(LED_PIN, data->toPinLevel());
}

void colorIncrease() {
  for(int i=0; i<100; i++) {
    setBrightnessAll(i);
    delay(10);
  }
}
void colorDecrease() {
  for(int i=100; i>0; i--) {
    setBrightnessAll(i);
    delay(10);
  }
}

void setBrightnessAll(int brightness) {
  for(int i=0; i<NUMPIXELS; i++) { 
          pixels.setPixelColor(i, pixels.Color(128, 0, 128));
          pixels.setBrightness(brightness);
    }
    pixels.show();   // Send the updated pixel colors to the hardware.

}
